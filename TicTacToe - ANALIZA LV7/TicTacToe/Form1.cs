﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    public partial class TicTacToe : Form
    {
        Graphics g;
        List<Pen> pens = new List<Pen>();
        Board play = new Board();
        int movesMade = 0;
        int ticTac;
        int bodX = new int();
        int bodY = new int();
        int pokretanje = 0;
        
        

        public TicTacToe()
        {
            InitializeComponent();
            g = pictureBox1.CreateGraphics();
            pens.Add(new Pen(Color.Black, 5F));
            bodX = 0;
            bodY = 0;
            
        }

        

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (pokretanje != 0) {
            movesMade++;
            if (movesMade % 2 != 0)
            {
                ticTac = 1;     //X
                label3.Text = "Trenutno na potezu: " + textBox2.Text;


            }
            else
            {
                ticTac = 2;     //O
                label3.Text = "Trenutno na potezu: " + textBox1.Text;
            }
            //MessageBox.Show("koordinate: " + e.X.ToString() + " " + e.Y.ToString());
            if (e.Y <= 500)
            {
                if (e.X < 167)
                {
                    if (e.Y < 167)
                    {
                        play.setClick(0, 0, ticTac);
                        if (ticTac == 1)
                        {
                            g.DrawLine(pens[0], 0, 0, 167, 167);
                            g.DrawLine(pens[0], 0, 167, 167, 0);
                        }
                        else if (ticTac == 2)
                        {
                            g.DrawEllipse(pens[0], 0, 0, 167, 167);
                        }
                    }
                    else if (e.Y < 334 && e.Y > 167)
                    {
                        play.setClick(0, 1, ticTac);
                        if (ticTac == 1)
                        {
                            g.DrawLine(pens[0], 0, 167, 167, 334);
                            g.DrawLine(pens[0], 0, 334, 167, 167);
                        }
                        else if (ticTac == 2)
                        {
                            g.DrawEllipse(pens[0], 0, 167, 167, 167);
                        }
                    }
                    else if (e.Y > 334)
                    {
                        play.setClick(0, 2, ticTac);
                        if (ticTac == 1)
                        {
                            g.DrawLine(pens[0], 0, 334, 167, 500);
                            g.DrawLine(pens[0], 0, 500, 167, 334);
                        }
                        else if (ticTac == 2)
                        {
                            g.DrawEllipse(pens[0], 0, 334, 167, 167);
                        }
                    }
                }
                else if (e.X < 334 && e.X > 167)
                {
                    if (e.Y < 167)
                    {
                        play.setClick(1, 0, ticTac);
                        if (ticTac == 1)
                        {
                            g.DrawLine(pens[0], 167, 0, 334, 167);
                            g.DrawLine(pens[0], 167, 167, 334, 0);
                        }
                        else if (ticTac == 2)
                        {
                            g.DrawEllipse(pens[0], 167, 0, 167, 167);
                        }
                    }
                    else if (e.Y < 334 && e.Y > 167)
                    {
                        play.setClick(1, 1, ticTac);
                        if (ticTac == 1)
                        {
                            g.DrawLine(pens[0], 167, 167, 334, 334);
                            g.DrawLine(pens[0], 167, 334, 334, 167);
                        }
                        else if (ticTac == 2)
                        {
                            g.DrawEllipse(pens[0], 167, 167, 167, 167);
                        }
                    }
                    else if (e.Y > 334)
                    {
                        play.setClick(1, 2, ticTac);
                        if (ticTac == 1)
                        {
                            g.DrawLine(pens[0], 167, 334, 334, 500);
                            g.DrawLine(pens[0], 167, 500, 334, 334);
                        }
                        else if (ticTac == 2)
                        {
                            g.DrawEllipse(pens[0], 167, 334, 167, 167);
                        }
                    }
                }
                else if (e.X > 334)
                {
                    if (e.Y < 167)
                    {
                        play.setClick(2, 0, ticTac);
                        if (ticTac == 1)
                        {
                            g.DrawLine(pens[0], 334, 0, 500, 167);
                            g.DrawLine(pens[0], 334, 167, 500, 0);
                        }
                        else if (ticTac == 2)
                        {
                            g.DrawEllipse(pens[0], 334, 0, 167, 167);
                        }
                    }
                    else if (e.Y < 334 && e.Y > 167)
                    {
                        play.setClick(2, 1, ticTac);
                        if (ticTac == 1)
                        {
                            g.DrawLine(pens[0], 334, 167, 500, 334);
                            g.DrawLine(pens[0], 334, 334, 500, 167);
                        }
                        else if (ticTac == 2)
                        {
                            g.DrawEllipse(pens[0], 334, 167, 167, 167);
                        }
                    }
                    else if (e.Y > 334)
                    {
                        play.setClick(2, 2, ticTac);
                        if (ticTac == 1)
                        {
                            g.DrawLine(pens[0], 334, 334, 500, 500);
                            g.DrawLine(pens[0], 334, 500, 500, 334);
                        }
                        else if (ticTac == 2)
                        {
                            g.DrawEllipse(pens[0], 334, 334, 167, 167);
                        }
                    }
                }
            }
            play.checkState(bodX, bodY, textBox1.Text, textBox2.Text, g);
            bodX = play.getBodX();
            bodY = play.getBodY();
        }
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawLine(pens[0], new Point(167, 0), new Point(167, 500));
            e.Graphics.DrawLine(pens[0], new Point(334, 0), new Point(334, 500));
            e.Graphics.DrawLine(pens[0], new Point(0, 167), new Point(500, 167));
            e.Graphics.DrawLine(pens[0], new Point(0, 334), new Point(500, 334));
        }

        private void pictureBox1_Layout(object sender, LayoutEventArgs e)
        {
            g.DrawLine(pens[0], 167, 1, 167, 499);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(textBox1.Text != String.Empty && textBox2.Text != String.Empty)
            {
                pokretanje++;
            }
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            label3.Text = "Trenutno na potezu: " + textBox1.Text;
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            label3.Text = "Trenutno na potezu: " + textBox1.Text;
        }
    }

    class Board : Form
    {
        private int[,] slots = new int[3, 3];
        private int xbod, ybod;

        public Board()
        {
            for(int i=0; i<3; i++)
            {
                for(int j=0; j<3; j++)
                {
                    slots[i, j] = 0;
                }
            }
        }

        public void setClick(int i, int j, int value)
        {
            slots[i, j] = value;
        }

        public void checkState(int x, int y, string strx, string stry, Graphics g)
        {
            int noWin=0;
            //provjera jeli je 3  x-a u redu:
            if(slots[0,0]==1 && slots[1,0] ==1 && slots[2, 0] == 1)
            {
                x++;
                MessageBox.Show("Pobjedio je " + strx);
                MessageBox.Show("Rezultat je: \n" + strx + " : " + x.ToString() + "\n" + stry + " : " + y.ToString());
                g.Clear(Color.White);
                g.DrawLine(new Pen(Color.Black, 5F), new Point(167, 0), new Point(167, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(334, 0), new Point(334, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 167), new Point(500, 167));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 334), new Point(500, 334));
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        slots[i, j] = 0;
                    }
                }
            }
            else if (slots[0, 1] == 1 && slots[1, 1] == 1 && slots[2, 1] == 1)
            {
                x++;
                MessageBox.Show("Pobjedio je " + strx);
                MessageBox.Show("Rezultat je: \n" + strx + " : " + x.ToString() + "\n" + stry + " : " + y.ToString());
                g.Clear(Color.White);
                g.DrawLine(new Pen(Color.Black, 5F), new Point(167, 0), new Point(167, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(334, 0), new Point(334, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 167), new Point(500, 167));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 334), new Point(500, 334));
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        slots[i, j] = 0;
                    }
                }
            }
            else if (slots[0, 2] == 1 && slots[1, 2] == 1 && slots[2, 2] == 1)
            {
                x++;
                MessageBox.Show("Pobjedio je " + strx);
                MessageBox.Show("Rezultat je: \n" + strx + " : " + x.ToString() + "\n" + stry + " : " + y.ToString());
                g.Clear(Color.White);
                g.DrawLine(new Pen(Color.Black, 5F), new Point(167, 0), new Point(167, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(334, 0), new Point(334, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 167), new Point(500, 167));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 334), new Point(500, 334));
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        slots[i, j] = 0;
                    }
                }
            }
            else if (slots[0, 0] == 1 && slots[0, 1] == 1 && slots[0, 2] == 1)
            {
                x++;
                MessageBox.Show("Pobjedio je " + strx);
                MessageBox.Show("Rezultat je: \n" + strx + " : " + x.ToString() + "\n" + stry + " : " + y.ToString());
                g.Clear(Color.White);
                g.DrawLine(new Pen(Color.Black, 5F), new Point(167, 0), new Point(167, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(334, 0), new Point(334, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 167), new Point(500, 167));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 334), new Point(500, 334));
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        slots[i, j] = 0;
                    }
                }
            }
            else if (slots[1, 0] == 1 && slots[1, 1] == 1 && slots[1, 2] == 1)
            {
                x++;
                MessageBox.Show("Pobjedio je " + strx);
                MessageBox.Show("Rezultat je: \n" + strx + " : " + x.ToString() + "\n" + stry + " : " + y.ToString());
                g.Clear(Color.White);
                g.DrawLine(new Pen(Color.Black, 5F), new Point(167, 0), new Point(167, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(334, 0), new Point(334, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 167), new Point(500, 167));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 334), new Point(500, 334));
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        slots[i, j] = 0;
                    }
                }
            }
            else if (slots[2, 0] == 1 && slots[2, 1] == 1 && slots[2, 2] == 1)
            {
                x++;
                MessageBox.Show("Pobjedio je " + strx);
                MessageBox.Show("Rezultat je: \n" + strx + " : " + x.ToString() + "\n" + stry + " : " + y.ToString());
                g.Clear(Color.White);
                g.DrawLine(new Pen(Color.Black, 5F), new Point(167, 0), new Point(167, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(334, 0), new Point(334, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 167), new Point(500, 167));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 334), new Point(500, 334));
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        slots[i, j] = 0;
                    }
                }
            }
            else if (slots[0, 0] == 1 && slots[1, 1] == 1 && slots[2, 2] == 1)
            {
                x++;
                MessageBox.Show("Pobjedio je " + strx);
                MessageBox.Show("Rezultat je: \n" + strx + " : " + x.ToString() + "\n" + stry + " : " + y.ToString());
                g.Clear(Color.White);
                g.DrawLine(new Pen(Color.Black, 5F), new Point(167, 0), new Point(167, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(334, 0), new Point(334, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 167), new Point(500, 167));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 334), new Point(500, 334));
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        slots[i, j] = 0;
                    }
                }
            }
            else if (slots[0, 2] == 1 && slots[1, 1] == 1 && slots[2, 0] == 1)
            {
                x++;
                MessageBox.Show("Pobjedio je " + strx);
                MessageBox.Show("Rezultat je: \n" + strx + " : " + x.ToString() + "\n" + stry + " : " + y.ToString());
                g.Clear(Color.White);
                g.DrawLine(new Pen(Color.Black, 5F), new Point(167, 0), new Point(167, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(334, 0), new Point(334, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 167), new Point(500, 167));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 334), new Point(500, 334));
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        slots[i, j] = 0;
                    }
                }
            }
            //provjera jeli su tri kruzica u redu
            if (slots[0, 0] == 2 && slots[1, 0] == 2 && slots[2, 0] == 2)
            {
                y++;
                MessageBox.Show("Pobjedio je " + stry);
                MessageBox.Show("Rezultat je: \n" + strx + " : " + x.ToString() + "\n" + stry + " : " + y.ToString());
                g.Clear(Color.White);
                g.DrawLine(new Pen(Color.Black, 5F), new Point(167, 0), new Point(167, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(334, 0), new Point(334, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 167), new Point(500, 167));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 334), new Point(500, 334));
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        slots[i, j] = 0;
                    }
                }
            }
            else if (slots[0, 1] == 2 && slots[1, 1] == 2 && slots[2, 1] == 2)
            {
                y++;
                MessageBox.Show("Pobjedio je " + stry);
                MessageBox.Show("Rezultat je: \n" + strx + " : " + x.ToString() + "\n" + stry + " : " + y.ToString());
                g.Clear(Color.White);
                g.DrawLine(new Pen(Color.Black, 5F), new Point(167, 0), new Point(167, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(334, 0), new Point(334, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 167), new Point(500, 167));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 334), new Point(500, 334));
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        slots[i, j] = 0;
                    }
                }
            }
            else if (slots[0, 2] == 2 && slots[1, 2] == 2 && slots[2, 2] == 2)
            {
                y++;
                MessageBox.Show("Pobjedio je " + stry);
                MessageBox.Show("Rezultat je: \n" + strx + " : " + x.ToString() + "\n" + stry + " : " + y.ToString());
                g.Clear(Color.White);
                g.DrawLine(new Pen(Color.Black, 5F), new Point(167, 0), new Point(167, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(334, 0), new Point(334, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 167), new Point(500, 167));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 334), new Point(500, 334));
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        slots[i, j] = 0;
                    }
                }
            }
            else if (slots[0, 0] == 2 && slots[0, 1] == 2 && slots[0,2] == 2)
            {
                y++;
                MessageBox.Show("Pobjedio je " + stry);
                MessageBox.Show("Rezultat je: \n" + strx + " : " + x.ToString() + "\n" + stry + " : " + y.ToString());
                g.Clear(Color.White);
                g.DrawLine(new Pen(Color.Black, 5F), new Point(167, 0), new Point(167, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(334, 0), new Point(334, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 167), new Point(500, 167));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 334), new Point(500, 334));
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        slots[i, j] = 0;
                    }
                }
            }
            else if (slots[1, 0] == 2 && slots[1, 1] == 2 && slots[1, 2] == 2)
            {
                y++;
                MessageBox.Show("Pobjedio je " + stry);
                MessageBox.Show("Rezultat je: \n" + strx + " : " + x.ToString() + "\n" + stry + " : " + y.ToString());
                g.Clear(Color.White);
                g.DrawLine(new Pen(Color.Black, 5F), new Point(167, 0), new Point(167, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(334, 0), new Point(334, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 167), new Point(500, 167));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 334), new Point(500, 334));
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        slots[i, j] = 0;
                    }
                }
            }
            else if (slots[2, 0] == 2 && slots[2, 1] == 2 && slots[2, 2] == 2)
            {
                y++;
                MessageBox.Show("Pobjedio je " + stry);
                MessageBox.Show("Rezultat je: \n" + strx + " : " + x.ToString() + "\n" + stry + " : " + y.ToString());
                g.Clear(Color.White);
                g.DrawLine(new Pen(Color.Black, 5F), new Point(167, 0), new Point(167, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(334, 0), new Point(334, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 167), new Point(500, 167));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 334), new Point(500, 334));
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        slots[i, j] = 0;
                    }
                }
            }
            else if (slots[0, 0] == 2 && slots[1, 1] == 2 && slots[2, 2] == 2)
            {
                y++;
                MessageBox.Show("Pobjedio je " + stry);
                MessageBox.Show("Rezultat je: \n" + strx + " : " + x.ToString() + "\n" + stry + " : " + y.ToString());
                g.Clear(Color.White);
                g.DrawLine(new Pen(Color.Black, 5F), new Point(167, 0), new Point(167, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(334, 0), new Point(334, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 167), new Point(500, 167));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 334), new Point(500, 334));
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        slots[i, j] = 0;
                    }
                }
            }
            else if (slots[0, 2] == 2 && slots[1, 1] == 2 && slots[2, 0] == 2)
            {
                y++;
                MessageBox.Show("Pobjedio je " + stry);
                MessageBox.Show("Rezultat je: \n" + strx + " : " + x.ToString() + "\n" + stry + " : " + y.ToString());
                g.Clear(Color.White);
                g.DrawLine(new Pen(Color.Black, 5F), new Point(167, 0), new Point(167, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(334, 0), new Point(334, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 167), new Point(500, 167));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 334), new Point(500, 334));
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        slots[i, j] = 0;
                    }
                }
            }

            //provjera jel je nerjeseno:
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if(slots[i, j] != 0)
                    {
                        noWin++;
                    }
                }
            }

            if(noWin == 9){
                MessageBox.Show("Nerjeseno je");
                MessageBox.Show("Rezultat je: \n" + strx + " : " + x.ToString() + "\n" + stry +  " : " + y.ToString());
                g.Clear(Color.White);
                g.DrawLine(new Pen(Color.Black, 5F), new Point(167, 0), new Point(167, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(334, 0), new Point(334, 500));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 167), new Point(500, 167));
                g.DrawLine(new Pen(Color.Black, 5F), new Point(0, 334), new Point(500, 334));
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        slots[i, j] = 0;
                    }
                }
            }

            noWin = 0;
            xbod = x;
            ybod = y;
        }

        public int getBodX()
        {
            return xbod;
        }

        public int getBodY()
        {
            return ybod;
        }
    }
}
